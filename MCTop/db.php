<?php

class Db {

    private $connection;
    private $status;

    const STATUS_CONNECTED = 1;
    const STATUS_PROBLEMS_WITH_CONNECTION = 0;

    public function __construct()
    {
        try {
            $this->connection = new PDO('mysql:host='.Core::getSettings()['db']['host'].';dbname='.Core::getSettings()['db']['dbname'], Core::getSettings()['db']['username'], Core::getSettings()['db']['password']);
            $this->connection->query("SET NAMES utf8");
            $this->status = self::STATUS_CONNECTED;
        }
        catch(PDOException $e) {
            $this->status = self::STATUS_PROBLEMS_WITH_CONNECTION;
            switch($e->getCode())
            {
                case 2002:
                    $message = 'Problems with connection to host: '.Core::getSettings()['db']['host'];
                    break;
                case 1049:
                    $message = 'Unknown database <b>'.Core::getSettings()['db']['dbname'].'</b>';
                    break;
                case 1045:
                    $message = 'Incorrect login or password';
                    break;
            }
            echo 'MCTop.Bonuses: '.$message;
        }
    }

    public function getConnection()
    {
        return $this->connection;
    }

    public function query($query)
    {
        return $this->connection->query($query);
    }

    public function queryFetch($query)
    {
        $query = trim($query);
        $result = array();
        $sth = $this->query($query);
        while( $row = $sth->fetch(PDO::FETCH_ASSOC) ) {
            $result[] = $row;
        }
        return $result;
    }

    public function getStatus()
    {
        return $this->status;
    }

}