<?php

    class Core {

        public $settings;
        public $connection;

        private static $_SETTINGS;
        private static $_DIRECTORY;
        private static $_DB;
        private static $_PLAYER;
        private static $_PLAYER_ACCOUNT;
        private static $_instance = null;

        private function __construct($directory) {
            // приватный конструктор ограничивает реализацию getInstance ()
        }

        protected function __clone() {
            // ограничивает клонирование объекта
        }

        static public function getInstance($directory) {

            if(is_null(self::$_instance))
            {

                if(sizeof($_GET)==0)
                {
                    echo 'MCTop.Bonus: Empty query.';
                    Core::End();
                }

                require_once('/../settings.php');
                require_once('db.php');

                self::$_instance = new self(self::$_DIRECTORY);
                self::$_SETTINGS = $conf;
                self::$_DIRECTORY = $directory;
                self::$_DB = new Db();

                if(self::$_DB->getStatus() == Db::STATUS_PROBLEMS_WITH_CONNECTION)
                    Core::End();
                self::$_PLAYER = $_GET['nickname'];

                if(self::isTestRequestFromMCTop())
                {
                    if(self::testIssuance())
                        echo json_encode(array('result'=>'success','message'=>'Script is working success'));
                    else
                        echo json_encode(array('result'=>'error','message'=>'The script settings was not properly set'));
                    Core::End();
                }

                if(isset($_GET['nickname']))
                {
                    self::$_PLAYER_ACCOUNT = self::getUser($_GET['nickname']);
                    if(is_null(self::$_PLAYER_ACCOUNT->balance))
                        throw new Exception('User not found');
                }

                else
                    throw new Exception('User not found');
            }

            return self::$_instance;
        }

        public static function getDb()
        {
            return self::$_DB;
        }

        public static function getSettings()
        {
            return self::$_SETTINGS;
        }

        public function isRequestFromMCTop()
        {
            $mctop_address = json_decode($this->sendApiQuery('http://mctop.su/leo/index.php?a=api&module=mctop&action=getMCTopAddress'));
            if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
                if($_SERVER['REMOTE_ADDR'] != $mctop_address->address)
                    throw new Exception('This request is not from MCTop');
        }

        public function isTestRequestFromMCTop()
        {
            if(isset($_GET['ajax'])) {
                if($_GET['ajax'] == md5((self::$_SETTINGS['secret_key'])))
                    return 1;
            }
        }

        public function testIssuance()
        {
            $user = self::getUser($_GET['nickname']);
            if(md5(Core::getSettings()['secret_key'])==$_GET['ajax'])
            {
                $user->ip = 'mctop';
                $user->token = 'mctop';
            }

            self::updatePlayerBalance();
            $user_second_check = self::getUser($_GET['nickname']);

            if($user->balance+Core::getSettings()['money_amount'] == $user_second_check->balance)
                return 1;
            else
                return 0;
        }

        public function CheckMultivote()
        {

            if(!$this->settings['allow_vote_from_1_ip'])
            {

                $yesterday = date('Y-m-d 00:00:00');

                $check = Core::getDb()->queryFetch("
                    select * from mctop_votes where ip='".$_GET['ip']."' and(time >= '{$yesterday}' and time < NOW()) order by id desc limit 1
                ");

                if(is_array($check) and sizeof($check)>0)
                    return 0;

                return 1;

            }
            else
                return 1;
        }

        public function checkTokens()
        {
            $legacy_token = md5(self::$_PLAYER.self::getSettings()['secret_key']);
            $incomming_token = $_GET['token'];
            if($legacy_token != $incomming_token)
                throw new Exception('Token does not match to possible legacy token');
            else
                return true;
        }

        public function updatePlayerBalance()
        {
            if(!self::isTestRequestFromMCTop())
                t();

            $update_balance =
                "
                    UPDATE ".self::getSettings()['db']['iconomy_table']."
                    set balance = balance + ".self::getSettings()['money_amount']."
                    where username = '".self::$_PLAYER."'
                ";

            if($_GET['ajax'] == md5((self::$_SETTINGS['secret_key'])))
                $_GET['ip'] = 'MCTop';

            $register_vote = "INSERT INTO mctop_votes (ip, nickname, time) values('".$_GET['ip']."','".self::$_PLAYER."', NOW())";

            Core::getDb()->getConnection()->query($register_vote);
            Core::getDb()->getConnection()->query($update_balance);
        }

        public static function getUser($login)
        {

            $data = Core::getDb()->queryFetch(
                "
                    select * from ".Core::getSettings()['db']['iconomy_table']." where username = '$login'
                ");

            if(sizeof($data)==0)
                if(!self::registerPlayer($login))
                    echo json_encode(array('result'=>'error','message'=>'Problem with saving user account in iConomy table'));
                else
                {
                    $data = Core::getDb()->queryFetch(
                        "
                            select * from ".Core::getSettings()['db']['iconomy_table']." where username = '$login'
                ");
                }

            if(Core::getSettings()['type'] == 'iConomy')
            {
                require_once('users/iConomyUser.php');
                $user = new iConomyUser();
                foreach($user as $key => $value)
                    $user->$key = $data[0][$key];
                $user->ip = $_GET['ip'];
                $user->token = $_GET['token'];
            }

            return $user;
        }

        public static function registerPlayer($login)
        {
            $registerPlayer = "INSERT INTO ".Core::getSettings()['db']['iconomy_table']." (username) values('{$login}')";
            if(Core::getDb()->getConnection()->query($registerPlayer))
                return true;
            return false;
        }

        public static function sendApiQuery($url)
        {
            $curlInit = curl_init();
            curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,1);
            curl_setopt($curlInit, CURLOPT_URL,($url));
            curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, '1');
            curl_setopt($curlInit,CURLOPT_TIMEOUT,1000);
            $result = curl_exec($curlInit);
            curl_close($curlInit);
            return $result;
        }

        public static function End()
        {
            die();
        }

    }
